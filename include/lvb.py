from __future__ import print_function

import time
import boto3
from datetime import datetime, timedelta
from botocore.client import Config

# increase request retries limit from the default of 4
config = Config(
    retries=dict(
        max_attempts = 10
    )
)

def list_tagged_volumes():
    """find all volumes with a tag of backup"""

    client = boto3.client('ec2')
    response = client.describe_volumes()
    tagged_volumes = []
    for volume in response['Volumes']:
        try:
            for tag in volume['Tags']:
                if tag['Key'].lower() == 'backup':
                    for v in volume['Attachments']:
                        vol = v['VolumeId']
                        instance = v['InstanceId']
                        mapping = v['Device']
                        backup_tag = tag['Value']
                        attachment_time = v['AttachTime'].date()
                        results = [vol, instance, mapping, backup_tag, attachment_time]
                        tagged_volumes.append(results)
        except Exception as e:
            print("no tags for " + str(volume))

    return tagged_volumes


def get_schedule(param_name):
    """read the a given ssm parameter for to retrieve a backup schedule"""

    ssm = boto3.client('ssm', region_name='eu-west-1')
    response = ssm.get_parameters(Names=[param_name], WithDecryption=True)
    try:
        sched = response['Parameters'][0]['Value']
        return sched
    except Exception as e:
        print(e)
        return "not_found"


def find_schedule(assigned_schedule, attachment_time):
    """ checks if backup os required for a given schedule and a volume attachment time """

    now = datetime.now().date()
    date_diff = (now - attachment_time).days + 1
    schedule = get_schedule(assigned_schedule)
    if schedule == "not_found":
        return ("not_found", 0)
    sched_child, sched_father, sched_grand = schedule.split(",")

    freq_grand = int(sched_grand.split(":")[1])
    freq_father = int(sched_father.split(":")[1])
    freq_child = int(sched_child.split(":")[1])
    ret_grand = int(sched_grand.split(":")[2])
    ret_father = int(sched_father.split(":")[2])
    ret_child = int(sched_child.split(":")[2])

    if date_diff % freq_grand == 0:
        return ("grandfather", ret_grand)

    if freq_father > 0:
        if date_diff % freq_father == 0:
            return ("father", ret_father)

    if freq_child > 0:
        if date_diff % freq_child == 0:
            return ("child", ret_child)

    return ("none", 0)


def create_snapshot(instance, volumeid, backup_type, expires, mapping):
    """creates snapshot for a given volume id. Tags the resulting snapshot with volumeid,
     backup type, snapshot expiry date and originating volume device mapping"""

    ec2 = boto3.resource('ec2')
    name = "Automated Lambda snapshot of " + volumeid
    description = volumeid + " - " + instance + " - " + \
        mapping + " - " + backup_type + " - " + expires
    snapshot = ec2.create_snapshot(VolumeId=volumeid, Description=description)
    snapshot.create_tags(Resources=[snapshot.id], Tags=[
        {'Key': 'mapping', 'Value': mapping},
        {'Key': 'expires', 'Value': expires},
        {'Key': 'instance', 'Value': instance},
        {'Key': 'volume', 'Value': volumeid},
        {'Key': 'Name', 'Value': name},
        {'Key': 'type', 'Value': backup_type}
    ])
    # Avoid the rate limit between concurrent volume snapshots (15 sec)
    time.sleep(15)

def delete_snapshots():
    """ tidy up expired snapshots"""

    now = datetime.now().date()
    now = datetime.now()
    delete_count = 0

    client = boto3.client('ec2', config=config)

    # paginators when return results are greater than a 1000 objects
    paginator = client.get_paginator('describe_snapshots')

    page_iterator = paginator.paginate(Filters=[{'Name': 'tag:Name', 'Values': ['Automated Lambda snapshot of*']}])

    for page in page_iterator:
        if 'Snapshots' in page:
            for snapshot in page['Snapshots']:
                for tag in snapshot["Tags"]:
                    if tag['Key'] == 'expires' and tag['Value'] != 'never':
                        string_date = tag['Value']
                        expire_date = datetime.strptime(string_date, "%Y-%m-%d")
                        diff = (expire_date - now).days + 1
                        if diff <= 0:
                            print ("deleting " + snapshot['SnapshotId'])
                            client.delete_snapshot(SnapshotId=snapshot['SnapshotId'])
                            delete_count += 1
    return delete_count

def snapshot_check(volumeid):
    """ counts existing snapshot generated. Used for creating the initial snapshot"""

    ec2 = boto3.resource('ec2')
    snapshots = ec2.snapshots.filter(Filters=[{'Name': 'tag:volume', 'Values': [volumeid]}])
    snapshot_count = len(list(snapshots))
    return snapshot_count


def lambda_handler(event, context):
    """main function for backup solution"""

    volumes_to_backup = list_tagged_volumes()
    backedup = 0
    for volume in volumes_to_backup:
        volumeid = volume[0]
        attachment_time = volume[4]
        assigned_schedule = volume[3]
        mapping = volume[2]
        instance = volume[1]
        if snapshot_check(volumeid) > 0:
            schedule = find_schedule(assigned_schedule, attachment_time)
            backup_type = schedule[0]
            if backup_type != "not_found":
                if backup_type != "none":
                    backedup += 1
                    if schedule[1] != 0:
                        expires = str(datetime.now().date() + timedelta(days=schedule[1]))
                    else:
                        expires = "never"
                    print("started snapshot for " + volumeid)
                    create_snapshot(instance, volumeid, backup_type, expires, mapping)
            else:
                print("no schedule found for " + volumeid +
                " - check ssm parameter has been created for " + assigned_schedule)
        else:
            backedup += 1
            print("initial snapshot taken for " + volumeid)
            create_snapshot(instance, volumeid, "initial", "never", mapping)

    cleanup = delete_snapshots()
    status = str(backedup) + " - volumes backed up   " + \
        str(cleanup) + " - snapshots removed"
    return status
