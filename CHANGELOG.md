## 1.0.1 (March 18, 2018)

* Removed envtype as the module doesn't require it

## 1.0.0 (August 23, 2017)

IMPROVEMENTS:
* Standardised name format for Lambda function

BUG FIXES:
* Fixed relative path to prevent Terraform from 'flapping' betweeen runs from multiple users


## 0.1.1 (August 21, 2017)

IMPROVEMENTS:
* Expanded Readme example for clarity
* Replaced `null_resource` with `source_code_hash` for lambda function


## 0.1.0 (July 04, 2017)

IMPROVEMENTS:
* Pinned project Terraform version to "~> 0.9"


## 0.0.4 (May 08, 2017)

IMPROVEMENTS:
* Updated Lambda file to improve readability and format
* Updated to apply best practice guidelines from PEP8


## 0.0.3 (April 28, 2017)

IMPROVEMENTS:
* Added non-expiring snapshot support
* Standardised variable names


## 0.0.2 (April 28, 2017)

IMPROVEMENTS:

* Tidied up resource names
* Tidied up variable names


## 0.0.1 (April 27, 2017)

Initial version

