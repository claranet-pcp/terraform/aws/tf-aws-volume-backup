tf-aws-volume-backup
========================

This module is for creating backups of volumes in AWS. It is triggered by a Cloudwatch schedule (volume_backup) that launches a Lambda function (volume_backup).

Rather than target all volumes aon a given instance, the Lambda targets tagged volumes, and links them to a backup schedule stored in an SSM Parameter.

Schedule - Stored as an SSM Parameter

A schedule is made up of 3 types of backup, Child, Father & Grandfather.
Each type has a backup frequency and a retention period.
Multiple schedules can be created in multiple SSM Parameters

The format of the schedule is:-

C:backup_frequency:backup_retention,F:backup_frequency:backup_retention,G:backup_frequency:backup_retention

for example,

An SSM Parameter called Backup_Schedule1 with a value of .....

C:1:14,F:7:28,G:28:84

..... would create a daily child backup that would be reatin for 2 weeks, a father backup every week that would be retain for 2 weeks, and a grandfather backup every 4 weeks and retained for 12 weeks.

backup dates are calculated from the date the volume was attached to an instance.

Once complete, the snapshots are tagged with the following:-

mapping - device mapping from originating volume
expires - date the snapshot is deleted
instance - originating instance
volume - originating volume
type - child/father/grandfather

## Terraform version compatibility

| Module version | Terraform version | Python Version |
|----------------|-------------------|----------------|
| 2.1.x          | 0.12.x            | 3.8            |
| 2.0.x          | 0.12.x            | 2.7            |
| <= 1.x.x       | 0.11.x            | 2.7            |

Prerequisites
-------------

There are some specific requirements when using this module, these are listed below;

- ssm parameter(s) created for schedule as defined above
- volumes to be backed up should have the tag **backup**, with a value matching one of the schedules stored as an ssm parameter

Usage
-----

Declare a module in your Terraform file, for example:

```js
module "volume_backup" {
  source = "../modules/tf-aws-volume-backup"

customer                = "${var.customer}"
envname                 = "${var.envname}"
cron_schedule           = "cron(0 1 * * ? *)"

}
```

Declare a schedule as a SSM parameter:

```js
resource "aws_ssm_parameter" "backup_param" {
  name  = "Backup_Schedule"
  type  = "String"
  value = "C:1:14,F:7:28,G:28:84"
}
```

And tag the volumes you wish the schedule to apply to:

```js
resource "aws_ebs_volume" "volume" {
  availability_zone = "eu-west-2a"
  size              = 100
  type              = "gp2"

  tags {
    backup = "Backup_Schedule"
  }
}
```

Variables
---------

- `customer`      - name of customer
- `envname`       - name of environment
- `cron_schedule` - crontab to trigger the Lambda - default set to daily 1am
