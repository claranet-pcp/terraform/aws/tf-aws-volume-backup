resource "aws_cloudwatch_event_rule" "volume_backup" {
  name                = "${var.customer}-${var.envname}-volume_backup"
  description         = "Trigger for lambda volume backup"
  schedule_expression = var.cron_schedule
}

resource "aws_iam_role" "iamforlambdabackup" {
  name = "${var.customer}-${var.envname}-iamforlambdabackup"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]

}
EOF
}

resource "aws_iam_policy" "volume_backup" {
  name        = "${var.customer}-${var.envname}-volume_backup"
  path        = "/"
  description = "volume_backup lambda policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": ["logs:*"],
      "Resource": "arn:aws:logs:*:*:*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "ec2:*",
        "ssm:*"
      ],
      "Resource": ["*"]
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "attachlogaccess" {
  name       = "${var.customer}-${var.envname}-AllowAccessToLogs"
  roles      = [aws_iam_role.iamforlambdabackup.name]
  policy_arn = aws_iam_policy.volume_backup.arn
}

resource "aws_lambda_function" "volume_backup" {
  filename         = ".terraform/lvb.zip"
  source_code_hash = data.archive_file.create_lambda_package.output_base64sha256
  function_name    = "${var.customer}-${var.envname}-lambda-volume-backup"
  role             = aws_iam_role.iamforlambdabackup.arn
  handler          = "lvb.lambda_handler"
  runtime          = var.python_version
  timeout          = "300"
  publish          = true
}

resource "aws_cloudwatch_event_target" "volume_backup" {
  rule = aws_cloudwatch_event_rule.volume_backup.name
  arn  = aws_lambda_function.volume_backup.arn

  input_path = "$.detail"
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_lambda_backup" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.volume_backup.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.volume_backup.arn
}

data "archive_file" "create_lambda_package" {
  type        = "zip"
  source_dir  = "${path.module}/include"
  output_path = ".terraform/lvb.zip"
}
