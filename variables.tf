/* Environment Variables */
variable "customer" {
  description = "This value will be the first name prefix on resources created by this module"
  type        = string
}

variable "envname" {
  description = "This value will be the second name prefix on resources created by this module"
  type        = string
}

/* Volume backup variables */
variable "cron_schedule" {
  description = "The schedule for running the job as a cron expression"
  type        = string
  default     = "cron(0 1 * * ? *)"
}

variable "python_version" {
  type    = string
  default = "python3.9"
}
